/*
 * Class sample main is published under the The MIT License:
 *
 * Copyright (c) 2022 Beamr Imaging Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import com.beamr.jpegmini.jni.JPEGmini;
import com.beamr.jpegmini.jni.JPEGminiResult;


public class Main {
    public static void main(final String[] args) throws IOException {
        System.out.println();
        System.out.println("JPEGmini JNI example");
        System.out.println("====================");

        if (args.length < 3) {
            System.out.println("Missing params - run with: license src.jpg out.jpg");
            return;
        }
        System.out.println("License     : "+args[0]);
        System.out.println("Input file  : "+args[1]);
        System.out.println("Output file : "+args[2]);

        try {
            // Load license
            String license = Files.readString(Path.of(args[0]));

            // Load input file into buffer
            File file = new File(args[1]);
            byte[] fileData = new byte[(int) file.length()];
            DataInputStream dis=null;

            dis = new DataInputStream(new FileInputStream(file));
            dis.readFully(fileData);
            dis.close();

            // Initialize JPEGmini
            JPEGmini jpegmini = new JPEGmini(license);

            // Optimize image
            int old_size = fileData.length;
            long t0 = System.nanoTime();
            JPEGminiResult result = (JPEGminiResult)jpegmini.compress(fileData, 0);
            if (result.resultcode!=0 ){
                System.out.println("JPEGmini process failed code="+result.resultcode);
                return;
            }
            long t1 = System.nanoTime();
            int new_size = result.output.length;
            System.out.println("JPEGmini took " + ((t1-t0)/1000000.0) + " milliseconds, Input size: " + old_size + ", Output size: " + new_size);

            // Write output buffer into file
            FileOutputStream fos = new FileOutputStream(args[2]);
            fos.write(result.output);
            fos.close();

            // Shutdown
            jpegmini.close();
        } catch (FileNotFoundException e) {
            System.out.println();
            System.out.println("Error   : File not found "+e.getMessage());
            System.out.println();
            return;
        } catch (IOException e) {
            System.out.println();
            System.out.println("Error:  :IO Error "+e.getMessage());
            System.out.println();
            return;
        }
    }
}
