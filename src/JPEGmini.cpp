/*
 * Class JPEGmini is published under the The MIT License:
 *
 * Copyright (c) 2022  Beamr Imaging Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "JPEGmini.h"
#include "JPEGminiAPI.h"

#include <cstring>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <iostream>
#include <fstream>
#include <assert.h>
#include <sys/stat.h>

using namespace std;

static jclass CompressionResults_class = 0;
static jmethodID CompressionResults_init = 0;
static jfieldID CompressionResults_output = 0;
static jfieldID CompressionResults_resultcode = 0;

/**
 * This method is called to init the Library services, should be
 * called one time.
 **/
JNIEXPORT jlong JNICALL Java_com_beamr_jpegmini_jni_JPEGmini_init
(JNIEnv *env, jobject, jstring license)
{
    // Initialize the library
    JMHANDLE jmlib = 0;

    const char* license_cstr = env->GetStringUTFChars(license, JNI_FALSE);
    int res = jm_trans_init_with_license(&jmlib, license_cstr);
    env->ReleaseStringUTFChars(license, license_cstr);

    if (JM_NOERR != res) {
        cerr << "jm_trans_init failed with result: " << res << endl;
        return 0;
    }
    assert (NULL != jmlib);

    jclass tmpCompressionResults_class = env->FindClass("com/beamr/jpegmini/jni/JPEGminiResult");
    CompressionResults_class = (jclass) env->NewGlobalRef(tmpCompressionResults_class);
    env->DeleteLocalRef(tmpCompressionResults_class);
    CompressionResults_init = env->GetMethodID(CompressionResults_class, "<init>", "()V");
    CompressionResults_output = env->GetFieldID(CompressionResults_class, "output", "[B");
    CompressionResults_resultcode = env->GetFieldID(CompressionResults_class, "resultcode", "I");
    //store library handle
    return (jlong)jmlib;
}

/**
 * This is the core compression method. We pass in a Java InputStream
 * which is a buffer to the JPEG image. We also pass in a Quality flag
 * to indicate the quality of the compression.
 **/
JNIEXPORT jobject JNICALL Java_com_beamr_jpegmini_jni_JPEGmini_compress
(JNIEnv * env, jobject self, jlong handle, jbyteArray data, jint quality)
{
    jint length = env->GetArrayLength(data);
    // Init the buffer to pass th JPEGMini
    JMHANDLE jmlib = (JMHANDLE)handle;
    JMIMGBUF *new_img = (JMIMGBUF *)0;
    JM_JPEG_HANDLE img = 0;
    unsigned char* buffer = (unsigned char*)env->GetByteArrayElements(data, NULL);
    int jm_error = 0;
    while (1)
    {
        int res = 0;
        res = jm_image_new(jmlib, buffer, (int)length, &img);
        if (0 != res)
        {
            cerr << "jm_image_new failed: " << res << endl;
            jm_error = res;
            break;
        }

        // modify these according to what is needed by your app
        float resizing              = 1; // resize ratio
        int fremove_metadata        = 0; // remove meta
        int fskip_highly_compressed = 1; // skip photos that are alreadt highly compressed

        res = jm_image_recompress(img, resizing, fremove_metadata,fskip_highly_compressed, (JM_QUALITY)quality, &new_img);
        if (0 != res) {
            cerr << "jm_image_recompress failed: " << res << endl;
            jm_image_free(img);
            jm_error = res;
            break;
        }
        break;
    }
    jobject results = env->NewObject(CompressionResults_class, CompressionResults_init, NULL);
    env->SetIntField(results, CompressionResults_resultcode, jm_error);
    if (!jm_error)
    {
        jm_image_free(img);
        jbyteArray array = env->NewByteArray(new_img->size);
        env->SetByteArrayRegion (array, 0, new_img->size, (jbyte*)(new_img->img));
        env->SetObjectField(results, CompressionResults_output, array);
        jm_free_img_buf(new_img);
    }
    env->ReleaseByteArrayElements(data, (jbyte*)buffer, JNI_ABORT);
    return results;
}

/**
 * If the Application or thread shutsdown we will invoke this method.
 **/
JNIEXPORT void JNICALL Java_com_beamr_jpegmini_jni_JPEGmini_close
(JNIEnv * env, jobject, jlong handle)
{
    jm_trans_fini((JMHANDLE)handle);
}


