/*
 * Class JPEGmini is published under the The MIT License:
 *
 * Copyright (c) 2016  Beamr Imaging Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package com.beamr.jpegmini.jni;

import java.io.IOException;

public class JPEGmini implements AutoCloseable {

    long handle = 0;

    static {
        // the JNI binding is a hybrid MacOS + Linux, so we check os, and try to load accordingly
        try 
        {


            if (System.getProperty("os.name").toLowerCase().contains("mac")) {
                // load tbb if it was included in the jar
                try{
                     NativeUtils.loadLibraryFromJar("/natives/libtbb.dylib");
                }
                catch (Exception e0) {
                }

                NativeUtils.loadLibraryFromJar("/natives/libjm.dylib");
                NativeUtils.loadLibraryFromJar("/natives/libjm_jni.so");
            } 
            else {

                // load tbb if it was included in the jar
                try{
                     NativeUtils.loadLibraryFromJar("/natives/libtbb.so.2");
                }
                catch (Exception e0) {
                }

                NativeUtils.loadLibraryFromJar("/natives/libjm.so");
                NativeUtils.loadLibraryFromJar("/natives/libjm_jni.so");                
            }

        }
        catch (IOException e1) {
            throw new RuntimeException(e1);
        }
    }

    public JPEGmini(String license) {
        handle = init(license);
        if (0 == handle) {
            throw new RuntimeException("Failed to initialize library");
        }
    }

    @Override
    public void close() {
        if (handle != 0) {
            close(handle);
            handle = 0;
        }
    }
    public JPEGminiResult compress(byte[] jpeg, int quality) {
        if (0 == handle) {
            throw new RuntimeException("Already closed");
        }
        JPEGminiResult result = (JPEGminiResult)compress(handle, jpeg, 0);
        return result;
    }

    // native method signature
    private native long init(String license);

    // native method signature
    private native Object compress(long handle, byte[] jpeg, int quality);

    // native method signature
    private native void close(long handle);

}
