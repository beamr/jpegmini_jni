/*
 * Class JPEGmini is published under the The MIT License:
 *
 * Copyright (c) 2022  Beamr Imaging Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef _Included_JPEG_Mini
#define _Included_JPEG_Mini

#include <jni.h>

#ifdef __cplusplus
extern "C" {
#endif

using namespace std;

/**
 * This method is called to init the Library services, should be
 * called one time.
 **/
JNIEXPORT jlong JNICALL Java_com_beamr_jpegmini_jni_JPEGmini_init
(JNIEnv *, jobject, jstring);

/**
 * This is the core compression method. We pass in a Java InputStream
 * which is a buffer to the JPEG image. We also pass in a Quality flag
 * to indicate the quality of the compression.
 *
 **/
JNIEXPORT jobject JNICALL Java_com_beamr_jpegmini_jni_JPEGmini_compress
(JNIEnv *, jobject, jlong handle, jbyteArray, jint);

/**
 * If the Application or thread shutsdown we will invoke this method.
 **/
JNIEXPORT void JNICALL Java_com_beamr_jpegmini_jni_JPEGmini_close
(JNIEnv *, jobject, jlong handle);

#ifdef __cplusplus
}
#endif

#endif

