Prerequisites
-------------
* JDK 11 or later
* g++ 
* execstack
* JPEGmini SDK (libjm.so)


Building
--------
* make sure JAVA_HOME points to JDK
* make sure JM_LICENSE points to your JPEGmini license file
* Copy libjm.so into build/natives (libjm.so is found in the JPEGmini SDK)

Make targets:
-------------
make jar - will create the jar without running anything
make test - will build everything and run sample code optimizing the jpg image found in sample/
make clean - removes all build products including out.jpg

