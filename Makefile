ifndef JAVA_HOME
    $(error JAVA_HOME is undefined)
endif

ifndef JM_LICENSE
    $(error JM_LICENSE is undefined. Use this env. variable to specify the license file)
endif


UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Linux)
	JM_LIB_NAME=libjm.so
	JAVA_OS_FOLDER=linux
endif
ifeq ($(UNAME_S),Darwin)
	JM_LIB_NAME=libjm.dylib
	JAVA_OS_FOLDER=darwin
endif

default: jar

init:
	mkdir -p build
	mkdir -p build/natives

jar: init jni
	javac -Xlint:deprecation -d build/ src/*.java
	cd build/ && jar cvf jpegmini.jar com/beamr/jpegmini/jni/JPEGmini.class com/beamr/jpegmini/jni/JPEGminiResult.class com/beamr/jpegmini/jni/NativeUtils.class natives/*.* && cd -

jni: init
	g++ -I${JAVA_HOME}/include/${JAVA_OS_FOLDER} -I${JAVA_HOME}/include/ -Iinclude/ -O0 -g3 -Wall -c -fmessage-length=0 -fPIC -MMD -MP -MFbuild/JPEGmini.d -MTbuild/JPEGmini.d -o build/JPEGmini.o src/JPEGmini.cpp
	g++ -fPIC -shared -o build/natives/libjm_jni.so  ./build/JPEGmini.o  build/natives/${JM_LIB_NAME}
	execstack -c build/natives/${JM_LIB_NAME}

test: jar
	javac -cp build/jpegmini.jar -Xlint:deprecation sample/Main.java
	java -cp sample/:build/jpegmini.jar Main ${JM_LICENSE} sample/rolands.lakis.1.jpg out.jpg

clean:
	rm -rf build
	rm -f sample/*.class
	rm -f out.jpg
